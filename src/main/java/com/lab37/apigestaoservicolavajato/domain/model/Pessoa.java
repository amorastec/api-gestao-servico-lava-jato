package com.lab37.apigestaoservicolavajato.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TBL_PESSOAS")
public class Pessoa {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PESSOA_ID_GENERATOR")
    @SequenceGenerator(name = "PESSOA_ID_GENERATOR", sequenceName = "PESSOA_ID_SEQ", allocationSize = 1)
    @Column(name = "PESSOA_ID")
    private Long id;

    @NotBlank
    @Column(name = "NAME")
    private String name;


}
