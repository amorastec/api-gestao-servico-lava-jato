package com.lab37.apigestaoservicolavajato.service.impl;

import com.lab37.apigestaoservicolavajato.domain.model.Pessoa;
import com.lab37.apigestaoservicolavajato.repository.PessoaRepository;
import com.lab37.apigestaoservicolavajato.service.PessoaService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PessoaServiceImp implements PessoaService {

    private final PessoaRepository pessoaRepository;

    @Override
    public List<Pessoa> getAll() {
        return pessoaRepository.findAll();
    }

    @Override
    public Pessoa getById(Long id) {
        return pessoaRepository.getById(id);
    }

    @Override
    public Pessoa save(Pessoa pessoa) {
        return pessoaRepository.save(pessoa);
    }

    @Override
    public void update(Long id, Pessoa pessoa) {

        var pessoaCurrent = pessoaRepository.getById(id);

        pessoaRepository.save(pessoaCurrent);

    }

    @Override
    public void delete(Long id) {
        var pessoaCurrent = pessoaRepository.getById(id);

        pessoaRepository.delete(pessoaCurrent);
    }
}
