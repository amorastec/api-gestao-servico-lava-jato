package com.lab37.apigestaoservicolavajato.service;

import com.lab37.apigestaoservicolavajato.domain.model.Pessoa;

import java.util.List;

public interface PessoaService {

    List<Pessoa> getAll();

    Pessoa getById(Long id);

    Pessoa save(Pessoa pessoa);

    void update(Long id, Pessoa pessoa);

    void delete(Long id);


}
