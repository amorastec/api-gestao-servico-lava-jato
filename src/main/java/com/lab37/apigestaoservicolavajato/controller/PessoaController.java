package com.lab37.apigestaoservicolavajato.controller;


import com.lab37.apigestaoservicolavajato.domain.model.Pessoa;
import com.lab37.apigestaoservicolavajato.service.PessoaService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("pessoas")
@AllArgsConstructor
public class PessoaController {

    private final PessoaService pessoaService;

    @GetMapping
    public ResponseEntity<List<Pessoa>> listAll() {
        return new ResponseEntity<>(pessoaService.getAll(), HttpStatus.OK);
    }

    @GetMapping("{pessoa_id}")
    public ResponseEntity<Pessoa> getById(@PathVariable("pessoa_id") Long idPessoa) {
        return new ResponseEntity<>(pessoaService.getById(idPessoa), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Pessoa> create(@RequestBody Pessoa pessoa) {
        return new ResponseEntity<>(pessoaService.save(pessoa), HttpStatus.CREATED);
    }

    @PutMapping("{pessoa_id}")
    public ResponseEntity<Void> update(@PathVariable("pessoa_id") Long idPessoa,
                                         @RequestBody Pessoa pessoa) {
        pessoaService.update(idPessoa, pessoa);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("{pessoa_id}")
    public ResponseEntity<Void> remove(@PathVariable("pessoa_id") Long idPessoa) {
        pessoaService.delete(idPessoa);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
