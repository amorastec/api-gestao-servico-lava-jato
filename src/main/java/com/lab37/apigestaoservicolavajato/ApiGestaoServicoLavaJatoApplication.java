package com.lab37.apigestaoservicolavajato;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiGestaoServicoLavaJatoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiGestaoServicoLavaJatoApplication.class, args);
	}

}
